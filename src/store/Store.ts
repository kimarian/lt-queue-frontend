import { combineReducers, createStore, applyMiddleware } from "redux"
import { createReducer } from "./Reducer"
import { AppState } from "./Type";
import { connectRouter, routerMiddleware } from "connected-react-router"
import History from "history"

export const createAppStore = (appInitialState: AppState, history: History.History) => {
    const rootReducer = combineReducers(
        {
            app: createReducer(appInitialState),
            router: connectRouter(history)
        }
    );
    return createStore(
        rootReducer,
        applyMiddleware(
            routerMiddleware(history)
        ));
}
