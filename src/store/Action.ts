import actionCreatorFactory, { AnyAction, Action } from "typescript-fsa";
import { LTDataList, LTData, fetchLTData, postLTData } from "../model/LTData";
import { AppState } from "./Type";
import { ThunkAction } from 'redux-thunk';
import { Dispatch } from "react";

const actionCreator = actionCreatorFactory();

export const SELECT_LT = "SELECT_LT";
export const selectLT = actionCreator<LTData>(SELECT_LT);

