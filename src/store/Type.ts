import { LTData } from "../model/LTData";
import { selectLT } from "./Action";

export interface AppState {
    selectedLT?: LTData;
}

export type Actions = (
    | ReturnType<typeof selectLT>
)

export const appInitialState : AppState = {
    selectedLT: undefined 
}

