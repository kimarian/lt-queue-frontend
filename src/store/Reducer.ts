import { AppState, Actions } from "./Type";
import { SELECT_LT } from "./Action";


export const createReducer = (initialState : AppState) => 
    (state: AppState = initialState, action: Actions) : AppState => {
        switch(action.type) {
            case SELECT_LT: 
                return {
                    ...state,
                    selectedLT: action.payload
                }
            default:
                return state;
        }
    }
