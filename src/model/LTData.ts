import { unstable_renderSubtreeIntoContainer } from "react-dom";

import { AppConfig, fetchURL, postURL, startURL, pauseURL, finishURL } from "../config/AppConfig"
import { sampleDatas } from "./DefaultData"

var currentPseudoSampleDatas : LTData[] = [];

export const operateLT = async (command: "start" | "finish", id : string) => {
    if (AppConfig.clientOnly) {
        const data = currentPseudoSampleDatas.map((x, i) => { return {value: x, index: i} }).filter(x => x.value.id == id)[0];
        const originalObject = data.value.toSendable();
        switch(command) {
            case "start":
                originalObject.timeData.startAt = new Date().getTime();
                originalObject.timeData.presenting = true;
                break;
            case "finish":
                originalObject.timeData.endAt = new Date().getTime();
                originalObject.timeData.presenting = false;
                break;
            default:
                break;
        }
        currentPseudoSampleDatas[data.index] = LTData.Convert(originalObject);
        return;
    } else {
        const url = command == "start" ? startURL(id) : finishURL(id);

        const res : Response = await fetch(url, {
            method: "GET",
            mode : "cors",
            cache: "no-cache",
            headers : {
                "Content-Type" : "application/json; charset=utf-8"
            }
        });
    }
}

export const fetchLTDatas = async (limit : number = 100) : Promise<LTData[]> => {
    // client test    
    if (AppConfig.clientOnly) {
        if (currentPseudoSampleDatas.length == 0) {
            currentPseudoSampleDatas = sampleDatas.map(x => LTData.Convert(x));
        }
        return currentPseudoSampleDatas;
    } else {
        // server test
        try {
            const res : Response = await fetch(fetchURL(), {
                    method: "GET",
                    mode : "cors",
                    cache: "no-cache",
                    headers : {
                        "Content-Type" : "application/json; charset=utf-8"
                    }
                });

            //console.log((await res.json() as any[]).map(x => LTData.Convert(x)));

            return (await res.json() as any[]).map(x => LTData.Convert(x));
        } catch(e) {
            throw e;
        }
    }
}

export const fetchLTData = async (id : string) : Promise<LTData> => {
    if (AppConfig.clientOnly) {
        if (currentPseudoSampleDatas.length == 0) {
            currentPseudoSampleDatas = sampleDatas.map(x => LTData.Convert(x));
        }
        return currentPseudoSampleDatas.filter(x => x.id == id)[0];
    } else {
        const datas = await fetchLTDatas();
        return datas.filter(x => x.id == id)[0];
    }
}

export const postLTData = async (ltdata: LTData) : Promise<void> => {
    if (AppConfig.clientOnly) {
        if (currentPseudoSampleDatas.length == 0) {
            currentPseudoSampleDatas = sampleDatas.map(x => LTData.Convert(x));
        }
        currentPseudoSampleDatas.push(ltdata);
        return;
    }

    try {
        const res : Response = await fetch(postURL(), {
                method: "POST",
                mode : "cors",
                cache: "no-cache",
                headers : {
                    'Accept': 'application/json',
                    "Content-Type" : "application/json; charset=utf-8"
                },
                body: JSON.stringify(ltdata.toSendable()),
            },
        );
    } catch(e) {
        throw e;
    }
}

export const getSamples: () => LTData[] = () => sampleDatas.map(x => LTData.Convert(x));

export class LTDataList {
    public readonly datas : LTData[];
    public readonly now : Date;

    private get today() : Date {
        return new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate());
    }

    public get todays() : LTData[] {
        return this.datas.filter(x => this.pickDating(x.timeData.date).getTime() == this.today.getTime())
    }

    public get presenting() : LTData[] {
        return this.datas.filter(x => x.timeData.presenting)
    }

    public get finished() : LTData[] {
        return this.datas.filter(x => x.status == LTDataStatus.Finished)
    }

    public get next() : LTData[] {
        let tomorrow = new Date(this.now);
        tomorrow.setDate(tomorrow.getDate() + 1);
        return this.datas.filter(x => tomorrow.getTime() <= x.timeData.date.getTime() && x.status != LTDataStatus.Finished)   
    }

    public get scheduled() : LTData[] {
        return this.datas.filter(x => x.status == LTDataStatus.Scheduled);
    }

    public constructor(datas: LTData[], now : Date) {
        this.datas = datas;
        this.now = now;
    }

    private pickDating(date : Date) : Date {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
}

export enum LTDataStatus {
    Scheduled,
    Presenting,
    Finished,
    Paused,
}

export class LTData {

    public readonly id? : string;
    public readonly content : LTContentData;
    public readonly timeData : LTTimeData;
    
    constructor(content : LTContentData, timeDate : LTTimeData, id?: string) {
        this.id = id;
        this.content = content;
        this.timeData = timeDate;
    }

    public get status() : LTDataStatus {
        return this.timeData.status;
    }

    public toSendable() : any {
        let sendable : any = {
            content : this.content as any,
            timeData : this.timeData.toSendable()
        }

        if (this.id != undefined) {
            sendable.id = this.id;
        }

        return sendable;
    }

    public static Convert(data : any) {
        if (!("content" in data) || !("timeData" in data)) throw "invalid data";

        return new LTData(
            LTContentData.Convert(data.content),
            LTTimeData.Convert(data.timeData),
            data.id
        );
    }
}


export class LTContentData {
    public readonly title : string;
    public readonly content : string;
    public readonly thumbnailUrl : string;

    public constructor(title : string, content : string, thumbnailUrl : string) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.content = content;
    }

    public static Convert(contentData: any) : LTContentData {
        return new LTContentData(
            contentData.title,
            contentData.content,
            contentData.thumbnailUrl
        );
    }
}


export class LTTimeData {
    public readonly date :  Date;
    public readonly duration : Date;
    public readonly presenting : boolean;
    public readonly startAt? : Date;
    public readonly endAt? : Date;

    public constructor(date : Date, 
            duration : Date, 
            presenting : boolean = false, 
            startAt? : Date, 
            endAt? : Date) {
        
        this.date = date;
        this.duration = duration;
        this.presenting = presenting;
        this.startAt = startAt;
        this.endAt = endAt;
    }

    public get status() : LTDataStatus {
        if (this.presenting) return LTDataStatus.Presenting;
        if (this.endAt != undefined) return LTDataStatus.Finished;
        if (this.startAt != undefined && !this.presenting) return LTDataStatus.Paused;
        return LTDataStatus.Scheduled;
    }

    public toSendable() : any {
        return {
            date : this.date.getTime(),
            duration : this.duration.getTime(),
            presenting : this.presenting,
            startAt : this.startAt == undefined ? -1 : this.startAt.getTime(),
            endAt : this.endAt == undefined ? -1 : this.endAt.getTime()
        };
    }

    public static Convert(timeData: any) : LTTimeData {

        let startAt = "startAt" in timeData ? new Date(timeData.startAt) : undefined;
        let endAt = "endAt" in timeData ? new Date(timeData.endAt) : undefined;        

        return new LTTimeData(
            new Date(timeData.date),
            new Date(timeData.duration),
            timeData.presenting,
            startAt,
            endAt
        );
    }
}
