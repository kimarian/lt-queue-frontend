export const sampleDatas = [
    {
        "id" : "0",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : true,
            "startAt" : new Date().getTime()
        }
    },
    {
        "id" : "1",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
            "startAt" : new Date(0, 0, 0, 0, 20).getTime(),
            "endAt" : new Date(0, 0, 0, 0, 40).getTime()
        }
    },
    {
        "id" : "2",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
    {
        "id" : "3",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
    {
        "id" : "4",
        "content" : {
            "title" : "TypeScriptをやろう！",
            "content" : "npm をインストール",
            "thumbnailUrl" : ""
        },
        "timeData" : {
            "date" : new Date().getTime(),
            "duration" : new Date(0, 0, 0, 0, 20).getTime(),
            "presenting" : false,
        }
    },
]