import React, { useState } from 'react';
import { Provider } from 'react-redux';
import './App.css';

import { CssBaseline, Box, Container } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import LinkView from "@material-ui/core/Link"
import { Route } from 'react-router-dom'
import history from "./history"
import Home from "./view/container/Home"
import LTList from "./view/container/LTList"
import LTDetail from "./view/container/Detail"
import Post from "./view/container/Post"
import { useStyles } from "./view/Style"
import { createAppStore } from './store/Store';
import { appInitialState } from './store/Type';
import { ConnectedRouter } from 'connected-react-router';
import { About } from './view/presentational/About';
import { Copyright } from './view/presentational/Copyright';

const store = createAppStore(appInitialState, history);

const App = () => {
  const classes = useStyles()
  return (
    <div>
      <Provider store={store} >
        <ConnectedRouter history={history}>
            <CssBaseline></CssBaseline>
            <AppBar position="relative">
              <Toolbar >
                  <Typography variant="h5" color="inherit" noWrap>
                      CalcQueue
                  </Typography>
                  <nav>
                    <LinkView variant="h6" color="inherit" className={classes.link} onClick={() => history.push("/about")}>About</LinkView>
                    <LinkView variant="h6" color="inherit" className={classes.link} onClick={() => history.push("/home")}>Home</LinkView>
                    <LinkView variant="h6" color="inherit" className={classes.link} onClick={() => history.push("/list")}>List</LinkView>
                    <LinkView variant="h6" color="inherit" className={classes.link} onClick={() => history.push("/post")}>Post</LinkView>
                  </nav>
              </Toolbar>
            </AppBar>
            <div>
              <Route exact path='/home' component={Home} />
              <Route path='/detail/:id' component={LTDetail} />
              <Route path='/list' component={LTList} />
              <Route path='/post' component={Post} />
              <Route path='/post/:id' component={Post} />
              <Route path="/about" component={About} />
            </div>
            <Container maxWidth="md" component="footer" className={classes.footer}>
              <Box mt={5}>
                <Copyright />
              </Box>
            </Container>
        </ConnectedRouter>
      </Provider>
    </div>
  );
}

export default App;
