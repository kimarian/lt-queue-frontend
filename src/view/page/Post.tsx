import React, { Fragment } from "react"
import { Dispatch, bindActionCreators } from "redux"
import { connect } from "react-redux"
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import moment from "moment";
import { CircularProgress } from "@material-ui/core";

import { useStyles } from "../Style"
import { LTData, postLTData, LTContentData, LTTimeData, fetchLTData } from "../../model/LTData"
import { push } from "connected-react-router";
import { RouteComponentProps, withRouter } from "react-router-dom";

interface IPostProp extends RouteComponentProps<{ id?: string }> {
    onCancel : () => void;
    toHome : () => void
}

type PostViewState = "initial" | "editting" | "posting" | "posted" | "fail" | "loading";

export const PostView = (prop : IPostProp) => {
    const classes = useStyles();

    const [postViewState, setPostViewState] = React.useState<PostViewState>("editting");
    const [existingData, setExistingData] = React.useState<LTData | undefined>(undefined);
    const [title, setTitle] = React.useState<string>("")
    const [thumbnailPath, setThumbnail] = React.useState<string>("")
    const [content, setContent] = React.useState<string>("")
    const [date, setDate] = React.useState<string>(moment().format("YYYY-MM-DD"));
    const [duration, setDuration] = React.useState<string>("00:20:00");

    // 既存のLTDataをばらまく
    const setFormState = (data : LTData) => {
        setExistingData(data);
        setTitle(data.content.title);
        setThumbnail(data.content.thumbnailUrl);
        setContent(data.content.content);
        setDate(moment(data.timeData.date).format("YYYY-MM-DD"));
        setDuration(moment(data.timeData.duration).format("KK-mm-ss"));
    }

    // フォームデータを集めてポストする
    const submitButtonHandler = () => {
        const inputtedDate : Date = moment(date).toDate();

        // 適当な日付をいれてパースできるようにする
        const inputtedDuration : Date = moment("2000-01-01 " + duration).toDate(); 
        
        let createdLTData: LTData;
        if (existingData == undefined) { // 完全に新規作成
            createdLTData = new LTData(
                new LTContentData(title, content, thumbnailPath),
                new LTTimeData(inputtedDate, inputtedDuration, false)
            );
        } else { // 編集                 　　 
            let timeData = existingData.timeData;
            createdLTData = new LTData(
                new LTContentData(title, content, thumbnailPath),
                new LTTimeData(inputtedDate, inputtedDuration, timeData.presenting, timeData.startAt, timeData.endAt),
                existingData.id
            );
        }

        setPostViewState("posting");
        postLTData(createdLTData)
        .then(x => {    
            setPostViewState("posted");
        }).catch(err => {
            setPostViewState("fail");
        })
    }

    // if ("id" in prop.match.params && postViewState == "initial") {
    //     let id = prop.match.params.id;
    //     if (id != undefined) {
    //         setPostViewState("loading");
    //         fetchLTData(id)
    //         .then(x => {
    //             setPostViewState("editting");
    //             setFormState(x);
    //         })
    //         .catch(e => setPostViewState("editting"));
    //     } else {
    //         setPostViewState("editting");
    //     }
    // } else {
    //     setPostViewState("editting");
    // }

    if (postViewState == "editting") {
        return (
        <React.Fragment>
            <Container className={classes.layout}>
                <Typography variant="h5" gutterBottom>
                    LT予約内容を入力
                </Typography>
                <Grid container spacing={4}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            variant="filled" required
                            id="title" name="title" label="タイトル"
                            fullWidth
                            autoComplete="fname"
                            value={title} onChange={x => setTitle(x.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <InputLabel>サムネイル</InputLabel>
                        <Input 
                            name="thumbnail"
                            type="file"
                            value={thumbnailPath} onChange={x => setThumbnail(x.target.value)}
                            />
                    </Grid>
                    <Grid item sm={12}>
                        <TextField
                            variant="filled" required
                            id="content" name="content" label="内容についての説明"
                            fullWidth autoComplete="fname"
                            rows="4"
                            multiline
                            value={content} onChange={x => setContent(x.target.value)}
                        />
                    </Grid>
                    <Grid container spacing={4}>
                        <Grid item xs={6} sm={2}>
                            <TextField
                                id="date" label="やりたい日" type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={date} onChange={x => setDate(x.target.value)}
                            />
                        </Grid>
                        <Grid item xs={6} sm={2}>
                            <TextField
                                id="time" label="発表時間" type="time"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                inputProps={{
                                    step: 500, 
                                }}
                                value={duration} onChange={x => setDuration(x.target.value)}
                            />
                        </Grid>
                    </Grid>
                    <Grid item sm={3}>
                        <div>
                            {/* <Button variant="contained" color="primary" onClick={() => prop.history.push("/")}>
                            投稿
                            </Button> */}
                            <Button variant="outlined" color="primary" onClick={() => prop.onCancel()}>
                            キャンセル
                            </Button>
                            <Button variant="contained" color="primary" onClick={() => submitButtonHandler()}>
                            投稿
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
        )
    } else if (postViewState == "posting") {
        return (
            <React.Fragment>
                <Typography variant="h5">
                    LTを投稿しています...。
                </Typography>
                <CircularProgress></CircularProgress>
            </React.Fragment>
        )
    } else if (postViewState == "posted") {
        setTimeout(x => prop.toHome(), 2000)
        return (
            <Fragment>
                <Typography variant="h5">
                    LTを投稿しました！
                </Typography>
            </Fragment>
        )
    } else if (postViewState == "loading" || postViewState == "initial") {
        return (
            <React.Fragment>
                <CircularProgress></CircularProgress>
            </React.Fragment>
        )
    } else {
        setTimeout(x => prop.toHome(), 2000)
        return (
            <Fragment>
                <Typography variant="h5" color="error">
                    投稿に失敗しました・・・。
                </Typography>
            </Fragment>
        )
    }
}

const mapDispatchToProps = (dispatch : Dispatch) => bindActionCreators({
    onCancel: () => push("/home"),
    toHome: () => push("/home")
    }, dispatch);

export default withRouter(connect(null, mapDispatchToProps)(PostView));
