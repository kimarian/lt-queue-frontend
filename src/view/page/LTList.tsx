import React, { useState } from 'react';
import { connect } from "react-redux";
import { Dispatch } from "redux"
import { LTData, fetchLTDatas, LTDataList } from "../../model/LTData"
import { useStyles } from "../Style"
import { LTListBlockViewFC } from "../presentational/LT"
import { Typography, CircularProgress } from '@material-ui/core';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

interface ILTListBlockProp {
    toDetail : (ltdata : LTData) => void;
}

export const LTListBlockView = (prop : ILTListBlockProp) => {
    const classes = useStyles();

    const [ltdatas, setLTDatas] = useState<LTData[]>([]);
    const [loadStatus, setLoadStatus] = useState<"initial" | "loading" | "complete" | "fail">("initial");
    
    console.log(loadStatus);

    if (loadStatus == "initial") {
        fetchLTDatas()
        .then(x => { console.log(x); setLTDatas(x); setLoadStatus("complete"); })
        .catch(e => { console.log(e); setLoadStatus("fail"); });
        setLoadStatus("loading");
    }

    const content 
        = loadStatus == "loading" ? (<CircularProgress />) :
          loadStatus == "fail" ?    (<Typography variant="h5" color="error"></Typography>) :
                                    (<LTListBlockViewFC ltdatas={ltdatas} toDetail={prop.toDetail}/>)

    return (
        <div>
            {content}
        </div>
    );
}

const mapDispatchToProps = (dispatch : Dispatch) => bindActionCreators({
    toDetail : (ltdata : LTData) => push(`/detail/${ltdata.id}`)
}, dispatch)

export default connect(null, mapDispatchToProps)(LTListBlockView);
