import React from "react"
import { Dispatch, bindActionCreators } from "redux"
import { connect } from "react-redux"
import { TitleView } from "../presentational/Title"
import { LTListBlockView } from "./LTList"
import { push } from "connected-react-router"
import { withRouter } from "react-router-dom"
import { LTData } from "../../model/LTData"

interface IHomeProps {
    toPost : () => void;
    toList : () => void;
    toDetail : (ltdata: LTData) => void;
}

export const HomeView = (prop: IHomeProps) => {
    return (
        <div>
            <TitleView  toPostPage={() => prop.toPost()} toListPage={() => prop.toList()}/>
            <LTListBlockView toDetail={x => prop.toDetail(x)}/>
        </div>
    )
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    toPost: () => push("/post"),
    toList: () => push("/list"),
    toDetail: (ltdata : LTData) => push(`/detail/${ltdata.id}`)
}, dispatch);   

export default withRouter(connect(null, mapDispatchToProps)(HomeView));
