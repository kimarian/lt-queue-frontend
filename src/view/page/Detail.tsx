import React, { useState } from "react"
import { Dispatch } from "redux"
import { connect } from "react-redux"
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Image from 'material-ui-image';
import moment from 'moment';

import { LTData, fetchLTData, operateLT, LTDataStatus } from "../../model/LTData"
import { LTStatusView } from "../presentational/LT"
import { useStyles } from "../Style"
import { bindActionCreators } from "redux";
import { push } from "connected-react-router";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";

interface LTDetailProp extends RouteComponentProps<{ id: string }>{
    toEdit : (ltdata: LTData) => void;
    onCancel : () => void; 
}

export const LTDetailView = (prop : LTDetailProp) => {
    const classes = useStyles();

    const [detailViewState, setDetailViewState] = useState<"initial" | "loading" | "loaded" | "fail">("initial")
    const [ltdata, setLTData] = useState<LTData | undefined>(undefined)

    if (detailViewState == "initial") {
        setDetailViewState("loading");
        fetchLTData(prop.match.params.id)
        .then(x => {
            setLTData(x)
            setDetailViewState("loaded");
        })
        .catch(e => setDetailViewState("fail"));

        return (
            <Container>
                <Typography variant="h5">
                    読み込んでいます...
                </Typography>
                <CircularProgress></CircularProgress>
            </Container>
        )
    } else if (detailViewState == "fail" || ltdata == undefined) {
        return (
            <Container>
                <Typography variant="h5">
                    なんらかのエラーが発生しました。
                </Typography>
            </Container>
        )
    } else if (detailViewState == "loading") {
        return (
            <Container>
                <Typography variant="h5">
                    読み込んでいます...
                </Typography>
                <CircularProgress></CircularProgress>
            </Container>
        )
    } else {
        const timeData = ltdata.timeData;
        const dateString = moment(timeData.date).format("YYYY年MM月DD日 kk時mm分 ");
        const durationString = moment(timeData.duration).format("HH時間mm分ss秒");

        const startLT = async () => {
            if (ltdata.id != undefined) {
                await operateLT("start", ltdata.id);
                setDetailViewState("initial"); // リロードする
            }
        };

        const stopLT = async () => {
            if (ltdata.id != undefined) {
                await operateLT("finish", ltdata.id);
                setDetailViewState("initial"); // リロードする
            }
        }

        return (
            <div>
                <Container className={classes.layout} maxWidth="md">
                    <Typography　variant="h4">詳細情報</Typography>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={4} md={4}>
                            <Typography　variant="subtitle1">サムネイル</Typography>
                            <img src="https://source.unsplash.com/random" style={{height:200}} />
                        </Grid>
                        <Grid item xs={12} sm={8} md={8}>
                            <Typography variant="subtitle1">タイトル</Typography>
                            <Typography variant="h5">{ltdata.content.title}</Typography>
                            <Typography variant="subtitle1">ステータス</Typography>
                            <LTStatusView ltdata={ltdata} />
                            <Typography variant="subtitle1">概要</Typography>
                            <Typography variant="body1" gutterBottom>
                                {ltdata.content.content}
                            </Typography>
                            <Typography>
                                発表予定日時 : {dateString}
                            </Typography>
                            <Typography>
                                発表予定時間 : {durationString}
                            </Typography>
                            <Typography variant="body1" color="textSecondary">
                                LTID: {ltdata.id} (システム管理者用)
                            </Typography>
                            <Grid item>
                                <Button className={classes.button} variant="contained" color="secondary" disabled={ltdata.timeData.presenting} onClick={() => startLT()}>
                                    {ltdata.timeData.startAt == undefined ? "開始！" : "再開"}
                                </Button>
                                <Button className={classes.button} variant="contained" color="default" disabled={!ltdata.timeData.presenting} onClick={() => stopLT()}>
                                    終了する
                                </Button>
                                <Button className={classes.button} variant="contained" color="primary" onClick={() => prop.toEdit(ltdata)}>
                                    編集する
                                </Button>
                                <Button className={classes.button} variant="outlined" color="primary" onClick={() => prop.onCancel()}>
                                    戻る
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
    toEdit : () => push("/post"),
    onCancel : () => push("/home")
}, dispatch);

export default withRouter(connect(null, mapDispatchToProps)(LTDetailView));
