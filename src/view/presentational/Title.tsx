import React from 'react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';

import { useStyles } from "../Style"

interface ITitleProp {
    toPostPage : () => void;
    toListPage : () => void;
}

export const TitleView = (prop : ITitleProp) => {
    const classes = useStyles();
    return (
        <div className={classes.heroContent}>
            <Container maxWidth="sm">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    CalcQueue
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    LTを活発に！
                </Typography>
                <div className={classes.heroButtons}>
                    <Grid container spacing={2} justify="center">
                    <Grid item>
                        <Button variant="contained" color="primary" onClick={() => prop.toPostPage()}>
                        LTを予約する
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button variant="outlined" color="primary" onClick={() => prop.toListPage()}> 
                        LTを一覧する
                        </Button>
                    </Grid>
                    </Grid>
                </div>
            </Container>
        </div>
    )
}