import React from "react"
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { useStyles } from '../Style';

export const About = () => {
    const classes = useStyles();
    return (
        <div className={classes.heroContent}>
            <Container maxWidth="sm">
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                    CalcQueue
                </Typography>
                <Typography variant="h5" align="center" color="textSecondary" paragraph>
                    LTを活発に！まだ文句きまってないので、あとで書く。
                </Typography>
            </Container>
        </div>
    )
}