import React, { useState, Fragment } from 'react';

import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import moment from "moment";
import "moment-duration-format";

import { LTData, LTDataList, LTDataStatus } from "../../model/LTData"
import { useStyles } from "../Style"

interface LTStatusProp {
    ltdata: LTData;
}

export const LTStatusView = ( prop: LTStatusProp ) => {

    const [toggle, tick] = useState<boolean>(false);
    setTimeout(() => tick(!toggle), 100);

    // この辺なんとかしたい。nullチェックをなんかいも挟むのツライ・・・。
    let startAt : Date | undefined = prop.ltdata.timeData.startAt;
    let endAt : Date | undefined = prop.ltdata.timeData.endAt;
    const startAtStr = startAt != undefined ? moment(startAt).format("YYYY年MM月DD日 HH:mm") : undefined;
    const endAtStr = endAt != undefined ? moment(endAt).format("YYYY年MM月DD日 HH:mm") : undefined;
    const elapsedTime = startAt != undefined ? moment(new Date()).diff(startAt.getTime()) : undefined;
    const elapsedTimeStr = startAt != undefined ? moment.utc(elapsedTime).format("HH:mm:ss") : undefined;
    const duration = startAt != undefined && endAt != undefined ?  moment(endAt.getTime()).diff(startAt.getTime()) : undefined;
    const durationStr = duration != undefined ? moment.utc(duration).format("HH:mm:ss") : undefined;

    switch(prop.ltdata.status) {
        case LTDataStatus.Finished: 
            if (startAtStr != undefined && endAtStr != undefined && durationStr != undefined)
            return (
                <Typography variant="body1" color="textSecondary">
                    終了 (開始時間: {startAtStr} 終了時間 : {endAtStr} 経過時間 : {durationStr})
                </Typography>
            );
        case LTDataStatus.Paused: 
            return (
                <Typography variant="body1" color="textSecondary">
                    中断！
                </Typography>
            );
        case LTDataStatus.Presenting:
            if (startAtStr != undefined && elapsedTimeStr != undefined) {
                return (
                    <Typography variant="body1" color="primary">
                        LT中！(開始時間: {startAtStr} 経過時間: {elapsedTimeStr})
                    </Typography>
                );
            } else {
                return (
                    <Typography variant="body1" color="primary">
                        LT中！
                    </Typography>
                ); 
            }
        default: 
            return (
                <Typography variant="body1" color="initial">
                    発表予定
                </Typography>
            ); 
    }
} 

interface ILTViewProp {
    ltdata : LTData;
    toDetail : (ltdata: LTData) => void;
    toEdit : (ltdata: LTData) => void;
}

export const LTView = (prop: ILTViewProp) => {
    const classes = useStyles();
    return (
        <Card className={classes.card}>
            <CardMedia
                className={classes.cardMedia}
                image="https://source.unsplash.com/random"
                title="Image title"
            />
            <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                    {prop.ltdata.content.title}
                    <LTStatusView  ltdata={prop.ltdata} />
                </Typography>
                <Typography className={classes.ellipsisText}>
                    {prop.ltdata.content.content}
                </Typography>
                <CardActions>
                    <Button size="small" color="primary" onClick={() => prop.toDetail(prop.ltdata)}>
                    View
                    </Button>
                    <Button size="small" color="primary" onClick={() => prop.toEdit(prop.ltdata)}>
                    Edit
                    </Button>
                </CardActions>
            </CardContent>
        </Card>
    );
}

interface ILTListViewProp {
    ltDatas : LTData[],
    toDetail : (ltdata: LTData) => void;
    toEdit : (ltdata: LTData) => void;
}

export const LTListView = (prop : ILTListViewProp) => {
    const classes = useStyles();
    return (
        <Container className={classes.cardGrid} maxWidth="md">
            {
                prop.ltDatas.map((ltdata) => 
                    <Grid item key={ltdata.id} xs={12} sm={12} md={12}>
                        <LTView ltdata={ltdata} 
                                toDetail={prop.toDetail} 
                                toEdit={prop.toEdit} 
                        />
                    </Grid>
                )
            }
        </Container>
    );
}

interface LTListBlockProp {
    ltdatas : LTData[];
    toDetail: (ltdata: LTData) => void;
}

export const LTListBlockViewFC = (prop : LTListBlockProp) => {
    const ltlist : LTDataList = new LTDataList(prop.ltdatas, new Date());

    return (
        <Fragment>
            <Typography variant="h5">presenting</Typography>
            <LTListView ltDatas={ltlist.presenting} toEdit={() => {}} toDetail={prop.toDetail} />
            <Typography variant="h5">todays</Typography>
            <LTListView ltDatas={ltlist.todays} toEdit={() => {}} toDetail={prop.toDetail} />
            <Typography variant="h5">scheduled</Typography>
            <LTListView ltDatas={ltlist.scheduled} toEdit={() => {}} toDetail={prop.toDetail} />
            <Typography variant="h5">past talks</Typography>
            <LTListView ltDatas={ltlist.finished} toEdit={() => {}} toDetail={prop.toDetail} />
        </Fragment>
    )
}