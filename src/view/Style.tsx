import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    icon: {
      marginRight: theme.spacing(2),
    },
    heroContent: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(8, 0, 6),
      textAlign: "left"
    },
    heroButtons: {
      marginTop: theme.spacing(4),
    },
    cardGrid: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
    card: {
      height: '100%',
      display: 'flex',
      margin: theme.spacing(1, 1, 1, 1)
    },
    cardMedia: {
        width: 151,
        height: 151
    },
    layout: {
      textAlign: "left",
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
    cardContent: {
        flexGrow: 1,
        width:'80%',
        textAlign: "left"
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    button: {
      margin: theme.spacing(1, 1, 1, 1)
    },
    image: {
      height: 10,
      width: "inherit"
    },
    ellipsisText: {
        overflow: "hidden",
        "text-overflow": "ellipsis",
        "white-space": "nowrap",
    },
    link: {
      margin: theme.spacing(1, 1.5),
    }
}));