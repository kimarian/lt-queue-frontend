import App from "../App";

export const AppConfig = {
    clientOnly : false,
    apiEndpoint : "http://localhost:5000"
};

const toURL = (suffix : string) => AppConfig.apiEndpoint + suffix

export const fetchURL = () => toURL("/lt")
export const postURL =  () => toURL("/lt/post")
export const editURL =  () => toURL("/lt/edit")
export const startURL = (id : string) => toURL(`/lt/start?id=${id}`)
export const pauseURL = (id : string) => toURL(`/lt/pause?id=${id}`)
export const finishURL =  (id : string) => toURL(`/lt/finish?id=${id}`)
